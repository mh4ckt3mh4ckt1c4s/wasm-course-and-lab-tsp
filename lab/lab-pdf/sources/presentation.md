# Preparing and checking the lab {.unnumbered}

You will work inside a container. Make sure that you pulled the image named `docker.io/mh4ckt3mh4ckt1c4s/wasm-lab:latest`. You may use either the `docker` or `podman` command as you see fit.

Then, start the container with:

```sh
podman run --name wasm-lab -it docker.io/mh4ckt3mh4ckt1c4s/wasm-lab
```

In this lab, you will need to start several terminals within the same container. To do so, open a new terminal and use the command `podman exec -it wasm-lab bash`.

Check that the container is working. You should see folders named `0X_name_of_part` that will correspond to the lab content for each part of this lab. The other folders were for configuration purposes ; you can ignore them.

A bunch of "utility" commands are already installed inside this container, mainly to deal with viewing and editing text files: `bat`, `less`, `vi`, `nano`, `grep`, `wc`... 

# First steps into WebAssembly

## Compiling and using the Rust example

Into the directory `01_first_steps`, you will find a directory named `rust-example`. Go into this directory and inspect the code in `src/main.rs` (you have the `bat` and `less` commands available). What does it seem to do?

You can compile the example using:

```sh
cargo build
```

The output will be located at `target/debug/rust-example`. Use the `file` command to determine what it is, and try to run it.

Now we will compile for Wasm/WASI. To do this, you must first download the corresponding toolchain (or in this case, ensure it is up-to-date, as it should be already downloaded):

```sh
rustup target add wasm32-wasi
```

> Remark: We will use the 32 bits version of Wasm, but the 64 bits version is also available with `wasm64-wasi`.

Now compile with:

```sh
cargo build --target wasm32-wasi
```

Output will be in `target/wasm32-wasi/debug/rust-example.wasm`. Again, use the `file` command to determine what kind of file it is, and try to run it with different Wasm runtimes (you have `wasmtime`, `wasmer`, `wasmedge` and `iwasm` available).

## Exploring the resulting binary (static analysis)

There are two main set of tools to manipulate and explore Wasm binaries: [WABT](https://github.com/WebAssembly/wabt) for the official WebAssembly organization and [wasm-tools](https://github.com/bytecodealliance/wasm-tools) from the Bytecode Alliance. Some of these tools are overlapping in functionalities, or out of scope for this lab (such as the ones dealing with validation or specification testing), so only a part of each of them will be presented. Feel free to explore further the whole list of tools on your own.

### WABT (WebAssembly Binary Toolkit)

Start by browsing the documentation associated with the tools to familiarize with them.

One of the most useful tools is `wasm2wat`: it allows you to convert a Wasm binary to its textual WAT representation, as the name hints.

Use `wasm2wat` to get the WAT representation of `rust-example.wasm` and store it in a file. Take a look in it. Is it like you expected? Why? How can you explain it? Can you find the `give_primes` function?

The WAT (and the binary) is way bigger than the content of the `main.rs` code because it contains all the code needed to allocate memory, manage WASI calls, etc. This is ordinarily provided by the standard library for Linux or Windows applications with [dynamic linking](https://en.wikipedia.org/wiki/Dynamic_linker), which does not exist in Wasm binaries.


#### Name mangling

You can't find in the WAT file the exact names of the functions defined in `main.rs`? That's because of **name mangling**. Take a look at [what name mangling is](https://en.wikipedia.org/wiki/Name_mangling), and specifically the [Rust part](https://en.wikipedia.org/wiki/Name_mangling#Rust). You should find a way to disable name mangling for specific functions.

Modify the content of `src/main.rs` by adding the `#[no_mangle]` decorator the lines before the declarations of functions to disable name mangling. Then, recompile the application into Wasm. Use the `wasm2wat` command to get the WAT and search for the functions. Did something change?

*Disabling name mangling is an important preparation step of the next part, which will be about debugging Wasm binaries. Indeed, if the debugger is not able to find back where the functions are in the generated binary, it makes the debugging much more difficult (no straightforward breakpoints on functions for example)*

#### (OPTIONAL) Indirect calls

We mentioned in the presentation the potential danger that the `call_indirect` instruction represents. Our `rust-example` code does not seem to contain any function pointers, so we should be safe, right? Check by counting the number of `indirect_call` instructions in the WAT file by using the `grep` and `wc` commands.

You found `call_indirect` instructions? Maybe nearly a hundred of them? Again, that's because of all the code needed to manage memory and other aspects of the execution. The code that you do not see, but is still present in your final Wasm binary, does need `call_indirect` to work.

> You can also use `wasm-opcodecnt` to directly count Wasm instructions.

Try and experiment more with the different tools if you like, for example `wasm-decompile`. Can you easily read its output?

### wasm-tools

Start by browsing the documentation associated with the tools to familiarize with them.

We will use the `wasm-tools` suite to see what a minimal Wasm binary looks like. Go into the `minimal_wasm` folder and inspect the code of `minimal1.wat`. You should see one line:

```
(module)
```

This is the smallest WAT code possible: an empty Wasm module. Now transform the WAT into a Wasm binary using the `wasm-tools parse` command. Try to run this module with the different Wasm runtimes. What can you see?

Then, analyze its contents using the `wasm-tools dump` command. The resulting binary that you inspect is very small, only eight bytes:

- The first four are `0x0061736d`, or `\x00asm` in ASCII (you can see that with the `xxd` command), and are the [magic bytes](https://en.wikipedia.org/wiki/List_of_file_signatures) of WebAssembly.
- The other four are `0x01000000`, designing the version 1 of WebAssembly in little endian.

This is a little too empty, so try the same manipulations on `minimal2.wat` to see more about the Wasm internal structure. First take a look at the WAT code, then transform it into Wasm, try to run it with different runtimes, and finally analyze it.

The code in `minimal2.wat` is the following:

```wat
(module
  (func (export "main") (param) (result))
)
```

It represents a module in which there is an only function, named `main`, which is empty: it takes no parameters, returns nothing, and does nothing.

Taking a look at the output of the `wasm-tools dump` command, we see:

- The **Wasm header** with its magic bytes: it is identical to the previous one.
- The **type section**: it is a table that defines a list of function signatures. It has only one entry, `type 0`, corresponding to a function that takes no parameters and return no result.
- The **func section**: it is another table defining the list of existing functions and their matching signature. We see it contains only one function, numbered `func 0`, that is of `type 0`.
- The **export section**: the WAT `export` instruction tells which functions are designed to be called from outside the Wasm module (by the runtime for example). Other functions are for internal use and can't be called from outside the Wasm module. Only one function is exported here, with the name `main` and corresponding to the `func 0` we mentioned just before.
- The **code section**: finally, some real code! You see here the code of the `main` function... which is empty (but still takes 2 bytes!)

You can also see a summary of the size, location and content of these sections by using the `wasm-tools objdump` command.

Feel free to explore these examples with other tools or to try and modify them and analyze the differences.

The `wasm-tools` suite can not only work with Wasm modules, but also with Wasm **components** (with the `wasm-tools component` and `wasm-tools compose` commands). [The component model](https://component-model.bytecodealliance.org/) is a proposal for a superset of Wasm modules that can be assembled to work together, no matter the language in which they are written. You will have the opportunity to explore them in the last part of this lab.

# Switching to dynamic analysis: debugging Wasm

Enough with static analysis! Now we will dive into dynamic analysis by looking at a live Wasm program, and try to modify it while running!

## Preparing the debugging

As mentioned in the presentation, Wasm debugging is very young and not generally available. You will need to use a custom debugger and Wasm runtime: fortunately, they already have been compiled when preparing the lab image and are waiting for use to use them.

Check that the `iwasm` and `lldb` commands are available and working. `iwasm` is the Wasm runtime we will be using to be able to debug, and `lldb` is the debugger from the LLVM project that have been patched to support Wasm debugging.

Go into the `02_debugging` folder. You will find a `rust-example-debug` project that is very similar to the one you already experimented with, but with some additions that will ease our debugging:

- The `#[no_mangle]` decorator that we discovered before, on each function. That will ease our debugging by allowing us to designate the functions to the debugger using their names;
- Additional lines (commented for now) that will help us find the locations of variables in memory. 

Go into the `rust-example-debug` folder, then try to compile with:

```sh
cargo build --target wasm32-wasi
```

You can then try to run the resulting binary with `iwasm`. It should work without problems.

Now take a look at the `lldb` [commands](https://lldb.llvm.org/use/map.html). There are many, but the most important ones will be listed here for easier reference:

- `b <function_name or address>` (breakpoint): set a breakpoint at the beginning or `function_name` or at an address.
- `n` (next): continue the execution to the next line.
- `c` (continue): continue the execution.
- `process interrupt`: interrupt the execution of a running process.
- `x/<number>x <address>`: print `number` 32-bits values at an address.
- `x/<number>b <address>`: print `number` bytes at an address.
- `x/<number>i <address>`: print `number` instructions at an address.
- `memory write <address> <value>`: write the `value` in memory at the `address` location.

You will need multiple terminals for debugging. Refer to the configuration part if you are not sure how to proceed.

You are now nearly ready to debug your first Wasm program. In one terminal, start:

```sh
iwasm -g=127.0.0.1:1234 target/wasm32-wasi/debug/rust-example-debug.wasm
```

And in another one, start the debugger with `lldb`. Then connect `lldb` to the server provided by `iwasm` with:

```sh
process connect -p wasm connect://127.0.0.1:1234
```

**You need to wait for the `(lldb)` prompt to reappear. Note that this can take some time (up to one minute). Be patient!**

Once `lldb` ready, you will see the address of where the code is currently stopped, along with the instructions that will follow. You can now put a breakpoint on the `give_primes` function with `b give_primes` (thanks to the deactivation of name mangling).

## Exploring the execution

You can now continue the execution with `c`. The "hello world" is displayed in the `iwasm` terminal, and the execution stops at the beginning of the `give_primes` function. You can see that the debugger is able to tell you which line of code you are currently executing.

You can display the ten instructions that are next to be executed using the `x/10i <address>` where address is the address shown by `lldb`, next to `frame #0:`. Go line by line using `n` and visualize the instructions each time. Try to see if you can understand some instructions by comparing it to the code. For example, when you will reach the `loop` and `for` Rust instructions, you will see the equivalent Wasm `loop` instructions. Same thing with the Rust `if` instructions, that corresponds to a `br_if` Wasm instruction.

We can now try to examine the content of the memory, and even modify it. Usually, `lldb` allows to display the content of the variables by using the `p <variable>` address. However, this feature is broken as of now (you may try if you want, but `lldb` will probably crash). 

To address this issue, we will... cheat by modifying the program to make it display the memory address we want to look at. Uncomment the lines 8 to 11 of the source at `src/main.rs`, recompile, and repeat the debugging steps above. Stop after the added lines have been executed and the memory addresses displayed.

Now that we know where to look, let's use the `x/...` instructions to display the values of `n` and `prime`. On how many bytes are they stored? What are their values? Continue executing the code step by step and display the values each time. Can you see the values change? Is it coherent with the code lines you executed?

## Modifying the behavior of the program

`n` is an integer stored on 32 bits, and `prime` is a boolean stored on only one byte. Restart the debugging from the beginning and try to modify the program behavior. For example, we will modify the `prime` variable to make the program believe that 3 is not prime. Go line by line until line 12, the beginning of the loop. Go through the first iteration of the loop, you should see `2 is prime` displayed, and the execution be back at line 12. Check that `n` is indeed equal to 3 by printing it with the debugger.

Then, step until line 15 (after `prime` have been set to `true`). Check that `prime` is indeed true (its value should be `0x01`) and modify this value with `memory write <prime_address> 0x00` (this will set `prime` to false). Then continue the execution with `c`. You should see that the program did not display the line `3 is prime`. Congratulations, you just modified the behavior of a Wasm program dynamically!

Now, try to adapt these instructions to achieve the following:

- Make the program display that 4 is prime.
- Make the program display that 1337 is prime (for the curious, $1337 = 7 \times 191$)

You now know how to debug a Wasm program and change its behavior!

# Working with a vulnerable binary

Let's use you freshly acquired skills to write an exploit for a vulnerable binary. The goal of this part is to end up with an input that will make the program display something else than expected, even if the data displayed is supposed to be immutable according to the code. You will be less guided in this part, but feel free to refer to the previous part as most of the commands are the same.

The program we will be working on is a simple C program, that defines in advance what the best school is MIT, then ask if you are able to change that. Let's prove you are stronger than this program by overwriting the memory in which the name of the best school is saved, and make the program say that TSP is the best school (or whatever school you think is best, no judgement... But TSP will be taken as an example in this lab).

## Memory protections in classic binaries VS in Wasm

Before exploiting the vulnerability, let's focus on what is allowing the exploit in the first place. Interestingly, this exploit is unfeasible when compiling to a classic Linux binary with exactly the same code (the bug is still present however, and the binary is exploitable in other ways).

Head into the `03_vulnerable_binary` directory and inspect the `vulnerable.c` file. Can you see where the vulnerability is?

Start by compiling into a classic Linux binary:

```sh
clang vulnerable.c -o classic
```

Execute the resulting `classic` binary and play with it. Can you make it crash? What mechanism makes the program crash?

Check the protections of the binary using:

```sh
checksec --file=./classic
```

You see that the **STACK CANARY** protection is enabled. This is the default for `clang` and other compilers when they detect a possibility for a buffer overflow. This means that the `classic` binary we produced is not (easily) exploitable.

But we can still produce a classic binary that will lack the stack canaries protections, and see what we can do with it. Let's compile one with:

```sh
clang -fno-stack-protector vulnerable.c -o classic-unprotected 
```

Use the `checksec` command again on this new binary to confirm that the stack canary protection have been disabled. Now play again with this new binary. Can you make it crash? Is the crash message the same as before? Why? What mechanism makes the program crash this time?

Now that the stack canaries are disabled, this program is effectively vulnerable and can be exploited in many ways. It would be possible, for example, to make it display that TSP is the best school. However, we want to erase the existence of MIT within memory by writing over it. Is that possible here?

To give an answer, we will debug the program to see where the "MIT" string is stored into memory. We must first compile it with the debug symbols (`-g` option):

```sh
clang -fno-stack-protector -g vulnerable.c -o classic-debug
```

Let's debug this program with `lldb ./classic-debug`. You can set a breakpoint on the main function with `b main`, then start the execution with `r`. Execute the first line with `n` to that the `BEST_SCHOOL` variable gets loaded into memory. You can now print its memory location with `p BEST_SCHOOL`. It should give you an address and the corresponding string "MIT".

Now that you have the address, you can see in which memory region it was loaded and with which permission using the command `memory region <address>`. It should give you an output like this:

```
[0x0000555555556000-0x0000555555557000) r-- /home/lab/<snip> PT_LOAD[2]
```

The triplet `r--` is indicating the permissions of the memory, very much like what you can found in the output of `ls -l`. It means that this memory zone only have read permissions, and thus that it is impossible to write over it. This protection cannot be easily bypassed, as it is the Linux kernel itself that is enforcing it.

However, as we have seen in the presentation, the simpler Wasm memory model does not contain any mechanism for memory permissions. That means that this program should be exploitable when compiled into Wasm!

## Understanding the vulnerability

We will now explore the Wasm binary. You can compile it with:

```sh
/opt/wasi-sdk/bin/clang -g -Wl,--stack-first vulnerable.c -o vulnerable.wasm
```

The `-Wl,--stack-first` option is passed to the linker to arrange the memory layout to make the exploit easier for this lab.

You can now debug it using the setup we saw before. Start the debugger with:

```sh
iwasm -g=127.0.0.1:1234 --heap-size=655360 vulnerable.wasm
```

The `--heap-size` option will allow us to print variables within the debugger with `p <variable>` (this will work here as we are working a simple C program). Execute the first lines of the program then print the `input` and `BEST_SCHOOL` variables. Continue the execution, give an input to the program and print the `input` variable again. You should see that it now contains the input you provided.

By calculating the difference between the position in memory of `input` and `BEST_SCHOOL`, you will know the length of the input you need to provide to overwrite the `MIT` string. Sadly, if you try to provide a string that long, the program will probably crash. This is because of important data present in memory that is overwritten and makes the program crash when it continues its execution, before reaching the part where it prints what we want.

To bypass this problem, we can print all the memory content that we will overwrite and write it inside our exploit. That way, when we will overflow, the data overwritten will be replaced by... the same data, and the program should be able to continue its execution without trouble.

Now that the crash is fixed, we should be able to overwrite further until we modify the `MIT` string!

## Exploiting!

Now that we know what we need to create our exploit, we need to write it. As we will need to input raw data to the program, we will need to generate this input and feed it to the program. We cannot "type" input as we did before, as the data we want to input is not necessarily "writable" (we want to input raw bytes and not ASCII or Unicode). 

To generate this input, we can use Python to write the correct bytes in a file and pipe the content of this file into the input of the program, or write a Python inline script with `python -c '...'`. The first method is advised for easier troubleshooting. If you know how to use it, the `pwntools` library may be of use. If not, do not focus on it as the exploit can be written without libraries and `pwntools` will just add more complexity to the mix if you are not familiar with it.

Do not forget that you need to take into account little endian, so that your overwrite is in the good order. Do not hesitate to debug your exploit like this:

```sh
cat exploit.txt | iwasm -g=...
```

That will allow you to examine the content of the memory just after your exploit overwrote it, to check that it is indeed equal to the memory before overwrite.

Once your exploit is ready and working, test it without debug mode and on several Wasm runtimes to check that it was not a fluke! If you want to compare your exploit, or to find inspiration if you are stuck, there is a proposition of correction available [here](https://gitlab.com/mh4ckt3mh4ckt1c4s/wasm-course-and-lab-tsp/-/blob/main/lab/lab-container/exploit_part_3.py). It was working when the lab was written but may need adaptation depending on the version of the compiler and other things. If it does, you just need to copy the memory dumped by `lldb` into the exploit to make it working in your case.

Congratulations, you just exploited a real vulnerability inside a Wasm program! 

# (OPTIONAL) Going further: Wasm components

If you have reached this part and want to experiment further with Wasm, you can take a look at the Wasm [component model](https://component-model.bytecodealliance.org/). It is a proposition of extension of WebAssembly modules to create interoperable, reusable Wasm applications and libraries. This part will not be guided at all but will give you some ideas to explore:

- First, take a look at the whole documentation. Try to understand what Wasm components are able to do that a simple Wasm module cannot.
- Familiarize yourself with how to compile a language to WebAssembly. This lab contains examples with Rust and C, you may try to see if your favorite language supports Wasm.
- Try to transform a simple Wasm module into a Wasm component (use the `wasm-tools` suite).
- Create a component composed of two Wasm modules, if possible written in different languages.
- Write a component composed of :
  - a Wasm module that will take in input the name of the best school and exploit automatically the vulnerable module of the precedent part,
  - and this vulnerable module.
