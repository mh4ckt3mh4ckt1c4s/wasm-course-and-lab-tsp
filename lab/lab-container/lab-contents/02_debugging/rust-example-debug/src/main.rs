use std::{thread, time::Duration};

#[no_mangle]
pub fn give_primes() {
    println!("There are all the prime numbers : ");
    let mut n = 2;
    let mut prime = true;
    // let pointer_prime = &prime as *const _;
    // let pointer_n = &n as *const _;
    // println!("n is located at : {:?}", pointer_n);
    // println!("prime is located at : {:?}", pointer_prime);
    loop {
        for i in 2..n {
            prime = true;
            if n % i == 0 {
                prime = false;
                break;
            }
        }
        if prime {
            println!("{n} is prime.");
            thread::sleep(Duration::from_millis(1000));
        }
        n += 1;
    }
}

#[no_mangle]
fn main() {
    println!("Hello world, running from Wasm!");
    give_primes();
}
