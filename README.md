# Wasm course and lab

This is a course and lab I teached to last-year students at Télécom SudParis specializing in cybersecurity.

## Using the content

The slides and pdf of the lab have both been written using [this pandoc framework](https://gitlab.com/mh4ckt3mh4ckt1c4s/ultimate-pandoc-framework).

The pdf content may be accessed under the `output` directories. If you want to regenerate them, make your changes and use the `make` command. For more details on how to modify the PDFs, take a look at the framework's README.

The lab is consisting of a container making available everyting that is needed for the lab. A version is available [here](https://hub.docker.com/repository/docker/mh4ckt3mh4ckt1c4s/wasm-lab/general), but you can rebuild the image yourself using the available `Containerfile` (expect a few hours of build). The explanations on how to use the container image for the lab are available in the lab pdf instructions.

## License

All the content in this repository is under the [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license, including the source code under the `lab/lab-container` folder. See more in the [LICENSE](./LICENSE). The framework used for generating these slides is under the MIT license as specified inside [this repository](https://gitlab.com/mh4ckt3mh4ckt1c4s/ultimate-pandoc-framework).
