Thanks for your detailed answers.

> Not sure it makes sense to be able to checkpoint the wasm runtime. The easier solution would be if the wasm runtime supports checkpointing because the process we are trying to checkpoint is not running directly on Linux.

Indeed, for the checkpoint / restore use case, only checkpointing the wasm application would suffice. More precisely, we would need to save the binary + its internal state, i.e. the whole content of the wasm runtime virtual memory (some optimizations may be made by saving only parts of the wasm runtime virtual memory, but I do not think the gained performance will be significant, and I do not know the wasm specs well enough to be able to tell if it is even feasible).

The main obstacle I see to this is that the internal state of the wasm runtime virtual memory may depend on the runtime used. This means that checkpointing and restoring would depend on the wasm runtime embedded within crun, and also its version (which is, from what I know, not accessible once embedded within crun). It also means that the checkpointing functionality should be implemented within all the wasm runtimes who can run in containers (at least those who can be embedded within crun, which are from what I know, `wasmedge`, `wasmer` and `wasmtime`, with `wasmedge` being the more actively supported).

> One could say we should be able to checkpoint the runtime. That could be possible.

In fact, I am more interested in checkpointing the whole runtime / container than just the contents of the wasm app. I'm using checkpointing for forensic analysis purposes and having the possibility to take a look at the whole runtime instead of only the app seems more interesting as it can enable us to detect runtime compromising or breakout. 

Moreover, in the case on using checkpointing at large scale on containers, as the recent introduction of checkpointing to the Kubernetes world can allow us, we're looking at automating checkpointing and analysis to help detect compromises within containers. For this kind of automation (and many other use cases that may arise from the democratization of container checkpointing), having the same format to analyze for both classic and wasm containers would be essential. I'm not sure that checkpointing only the wasm application would allow to have a level of detail and flexibility comparable to the checkpointing of the whole container or runtime.

Finally, a wasm container can contain more than just the wasm app : configuration files, storage (database or other), other applications or libraries... Which may make checkpointing only the state of the wasm app less relevant. 

> The most confusing thing right now for me, besides the mount ID, is the fact that the runtime libraries are not part of the container which means that the restore will depend on the exact version of the host libraries installed and not on the content of the container.

crun is embedding a wasm runtime (only the core runtime). To be able to run a wasm container, the wasm runtime  library (typically `libwasmedge.so` for wasmedge) must be present on the system. Then, when detecting a wasm runtime, crun will delegate the running of the container to this wasm runtime, instead of running it directly on the host system as it is happening with classic containers. 

Indeed, this means that at contrary to classic containers, where all the binaries needed to run the containers (coreutils and more) are present within the container image, the host needing only to provide access to its kernel. For wasm, an external library (and more ?) is used. 

I don't know how container checkpointing works internally, and if it is supposed to depend on the `crun` or `libcrun.so` versions, or even if you can checkpoint a `crun` container and restore it on `runc` or another container runtime. Answers to these questions may guide us on how to implement the checkpointing for a wasm container and if it needs to be compatible between runtimes and runtimes versions. This should also be discussed with those who created the wasm support for crun as their point of view of the inner working of wasm workload delegation may be enlightening.

@Snorch : Thanks for the highlights. I only tried to checkpoint the container through podman or crun, and the `--external` flag doesn't seem available for these tools. I'll try to checkpoint a container using criu directly and specify the path to the wasm library in case this is the failing point. 

I'm also wondering how the wasm library is loaded from within the container. Is it mounted inside the container ? With the good permissions / namespaces / etc ?