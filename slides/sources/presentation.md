# Before starting {.unnumbered}

- I'm Quentin MICHAUD, PhD student at Télécom SudParis
- Feel free to interrupt for any questions
- Course and lab are both open (source) and available on my GitLab^[https://gitlab.com/mh4ckt3mh4ckt1c4s/wasm-course-and-lab-tsp] 

# Introducing WebAssembly

## What is it?

Wasm is short for **WebAssembly**. It is written Wasm and not WASM as it is not an acronym.^[https://webassembly.github.io/spec/core/intro/introduction.html#wasm]

----

- portable, low-level binary instruction format for a stack-based virtual machine
- First created as a JS complement to enable complex calculus in the browser (3D rendering, math, video games...)
- Able to reach near-native speeds (way better than JS)
- Needs JS to interact with the browser and the DOM
- Announced in 2015 and published in 2017

### Standardization

Wasm is a W3C standard and is in version 1.0 for now, which is implemented in all major browsers. Version 2.0 is on draft since 2022.

### Wasm outside the browser

Some people saw the potential of WebAssembly as a universal bytecode machine and decided to take it outside of the web world...

- By putting it **directly on a computer**
- As it is a low-level bytecode it can be used as **smart contracts** in blockchains, it's now one of the competitors of Solidity
- And using its sandbox capacities and lightweight design to replace **containers**

## What is WASI?

### Creation of WASI

Out of the browser, Wasm cannot rely anymore on JS to interact with the outside world. Something new was needed: it's **WASI**

. . .

::: block

#### WASI

WASI means **WebAssembly System Interface**. It is a set of standards to define how to compile native applications to standalone Wasm by giving definitions for standard OS interfaces.^[https://github.com/WebAssembly/WASI]
:::

---

A set of standards means, according to WASI:

\ 

> The WebAssembly System Interface is not a monolithic standard system interface, but is instead a **modular collection of standardized APIs**. **None of the APIs are required to be implemented** to have a compliant runtime.

### State of WASI

::: block
#### No standardization yet {.alert}

As of now (November 2023), **no API** is properly standardized by WASI!!
:::

. . .

**On the five phases needed to standardize an API within WASI, only six reached phase 2** (`Proposed Spec Text Available`), and 20+ are still in phase 1 (`Feature Proposal`).^[https://github.com/WebAssembly/WASI/blob/main/Proposals.md]

. . .

The six APIs which reached phase 2 are prioritized by WASI as they will lay the strict basis for OS interaction. They are `I/O`, `Filesystem`, `Clocks`, `Random`, `Poll`, and... `Machine Learning`?

. . .

Alongside them in phase 1 we find some more or less essential APIs, such as `Crypto`, `Sockets` or `Threads`.

## Drawbacks and promises

### Security implications of Wasm in the browser

- In 2018, a PoC using Wasm to **bypass browser mitigations for _Spectre_ and _Meltdown_** was published. However, it was relying on the threads feature which is not yet widely supported.
- It is widely used as a **cryptominer**, as it is less detectable and way faster than JS.
- It gives possibility to **obfuscate large amounts of code** and can be used to **bypass ad blocking or tracking prevention**.
- More generally, it allows running untrusted code in the users' browsers while **being more difficult to analyze** and block because of its compiled format.

### The future of computing and containerization?

Solomon Hykes, co-founder of Docker, wrote (emphasis mine):

\ 

> If WASM+WASI existed in 2008, **we wouldn't have needed to create Docker**. That's how important it is. **WebAssembly on the server is the future of computing**.

. . .

The CEO of a Wasm-related company, Syrus Akbary, said:

\ 

> By leveraging Wasm for software containerization, **we create universal binaries that work anywhere without modification**, including operating systems like Linux, macOS, Windows, and web browsers. **Wasm automatically sandboxes applications by default** for secure execution, shielding the host environment from malicious code, bugs, and vulnerabilities in the software it runs.

# Inner workings of Wasm

## Wasm runtimes

### What is needed?

In order to run Wasm, we need:

::: nonincremental

- A Wasm **runtime**
- A way to **interact with the outside world**: either JS in the browser, a JS runtime (node), or WASI.

:::

. . .

When using JS, a JS "glue code" is produced to load and start the Wasm app.

. . .

When using WASI, the Wasm runtime **embeds directly WASI implementations and deals with the OS interactions by itself**.

### Landscape of Wasm runtimes

There are **a lot of existing Wasm runtimes**^[https://github.com/appcypher/awesome-wasm-runtimes], for different target uses and implemented in different languages, mostly C, C++ or Rust.

. . .

The most popular are:

- `wasmtime`[^wasmtime] (original implementation by the Bytecode Alliance)
- `wasmedge`[^wasmedge] (more lightweight, designed for the cloud and edge)
- `wasmer`[^wasmer] (aimed at containers)
- `WAMR`[^WAMR] for IoT.

[^wasmtime]: https://github.com/bytecodealliance/wasmtime
[^wasmedge]: https://github.com/WasmEdge/WasmEdge
[^wasmer]: https://github.com/wasmerio/wasmer
[^WAMR]: https://github.com/bytecodealliance/wasm-micro-runtime

## Compilation

### Overview

*In theory*, you can compile to Wasm from any LLVM-based language. Practically however, the only well-supported languages for all the compilations targets we described before are C/C++ and Rust.

. . .

There are different ways to compile WebAssembly:

- **Emscripten**^[https://emscripten.org/]: the original way to compile for the web, but also supports WASI and implement its own APIs. Designed for C/C++.
- **wasi-sdk**^[https://github.com/WebAssembly/wasi-sdk]: the official Wasm / WASI LLVM-based toolchain.

---

- Language-specific compilers, such as **cargo** for Rust^[https://www.rust-lang.org/what/wasm] with specific targets such as `wasm32-wasi`. Some compilers support only a subset of the possible compilation targets, such as the go compiler which can only build for in-browser targets (WASI is in preview).

The official Wasm developers page^[https://webassembly.org/getting-started/developers-guide/] mentions the following list: C/C++, Rust, AssemblyScript, C#, Dart, F#, Go, Kotlin, Swift, D, Pascal, Zig and Grain.

### And interpreted languages?

When interpreted languages rely on a C or LLVM-based runtime, we can **compile the runtime into Wasm** and use it to run the targeted languages. This has already been done with Python^[https://wasmlabs.dev/articles/wasm-host-to-python/] and PHP^[https://wasmlabs.dev/articles/php-wasm32-wasi-port/] at various degrees of maturity.

### Compiling in C

We will focus on the official **wasi-sdk** toolchain with native targets.

Once installed, it provides the **LLVM compiler toolchain already configured for WASI** under the `bin` directory. You can then use these tools as for compiling for native targets.

With some tweaks, this toolchain can be adapted to building tools like `autoreconf` and `automake`, allowing to port easily to Wasm a wide panel of C/C++ applications.

---

A simple example using clang, supposing `wasi-sdk` is installed under the `/opt` directory:

```bash
$ echo '''
     #include<stdio.h>
     int main() {
         puts("Hello, WASI world!");
     }
     ''' > demo.c
$ /opt/wasi-sdk/bin/clang -o demo.wasm demo.c
$ wasmer demo.wasm 
Hello, WASI world!
```

### Compiling in Rust

The Rust developers put a lot of effort into supporting **building natively to Wasm targets**. With their packaging and build tool **cargo** and their version and toolchain manager **rustup**, Rust projects can be built into Wasm / WASI very easily.

```bash
# Creating the demo app 
$ cargo new demo && cd demo
# Installing or upgrading the wasm32-wasi toolchain
$ rustup target add wasm32-wasi
# Building and running
$ cargo build --target wasm32-wasi
   Compiling demo v0.1.0 (/tmp/poc2/app/demo)
    Finished dev [unoptimized + debuginfo] target(s) in 0.60s
$ wasmer target/wasm32-wasi/debug/demo.wasm 
Hello, world!
```

## Representing Wasm binary code

:::: block

### WebAssembly Text format

The **WebAssembly Text format** (WAT) is the text format used to represent the Wasm binary format. It is similar to the different flavors of assembly for x86 or other architectures.

::::

. . .

- More verbose and high-level
- Definition of functions, object naming, types
- Standardized!
- More info^[https://developer.mozilla.org/en-US/docs/WebAssembly/Understanding_the_text_format] and the spec^[https://webassembly.github.io/spec/core/text/index.html]

----

```asm
(func $fputs (type 3) (param i32 i32) (result i32)
    (local i32)
    local.get 0
    call $strlen
    local.set 2
    i32.const -1
    i32.const 0
    local.get 2
    local.get 0
    i32.const 1
    local.get 2
    local.get 1
    call $fwrite
    i32.ne
    select)
```

## Debugging a Wasm process

### How to do it?

- a Wasm "process"?
- only the Wasm runtime is visible from the OS
- debugging the runtime directly is painful

### WAMR and iwasm

WAMR^[https://github.com/bytecodealliance/wasm-micro-runtime] and its corresponding CLI `iwasm` is a Wasm runtime. However, and it seems to be the only one of its kind, it **comes with integrated support for debugging Wasm!**^[https://bytecodealliance.github.io/wamr.dev/blog/wamr-source-debugging-basic/]

- `iwasm` embeds a debugging server
- compile to Wasm with DWARF debugging symbols
- run binary with `iwasm`
- use a custom compiled `lldb` to connect to `iwasm` and debug

## Introducing Wasm security

The detailed position of Wasm regarding its security is explained on a specific page of its documentation^[https://webassembly.org/docs/security/]. Some extracts:

- *WebAssembly programs are protected from control flow hijacking attacks* (implicit CFI enforcement)
- *In the future, support for multiple linear memory sections and finer-grained memory operations will be implemented* (ASLR, page protections...)
- *common mitigations such as data execution prevention (DEP) and stack smashing protection (SSP) are not needed by WebAssembly programs*

## What about WASI security?

- From the browser sandbox to... nothing?
- WASI is still at a very early stage (no standardization yet)
- No evaluation of WASI security and runtimes exist yet to my knowledge

# Exploring Wasm/WASI security

## Memory inner workings

::: columns

:::: column
The Wasm user-addressable memory is a simple **linear, zero-initialized memory**. It does NOT have:

- Any **paging or mapping mechanism** that would introduce gaps in memory.
- Any mechanism for **pages or zones permissions**.
::::

:::: column
![A Linux process memory^[https://manybutfinite.com/post/anatomy-of-a-program-in-memory/]](./media/memory.png){width=90%}
::::
:::

----

Upsides and drawbacks:

- Wasm memory is easier to understand and manipulate
- The simplicity of the model restricts Wasm to a single thread and application (for now)
- Specific use cases may need the development of a specific WASI API

## The Security Risk of Lacking Compiler Protection in WebAssembly

This paper^[https://arxiv.org/abs/2111.01421] (Stiévenart et al., 2021) explored the **security implications of the Wasm memory model**.

The authors found out that lacking canaries in Wasm allows for **memory bugs** that are more present and more exploitable than in their ELF counterparts with SSP protections on. This shows that the assertion shown on the Wasm website is **fundamentally false**.

## Canaries in Wasm as of today

- the article is 2 years old and using **clang v11**
- **clang v16** today supports SSP
- findings of the article may no longer be true
- conclusion remains: **canaries are useful in Wasm**

## Everything Old is New Again: Binary Security of WebAssembly

This excellent article (Lehmann et al., 2020)^[https://www.usenix.org/system/files/sec20-lehmann.pdf] compares the feasibility of memory attacks in Wasm VS in classic binaries. It shows that Wasm not only **lacks protections present in native binaries**, but also enables for **new kind of attacks**. It concludes with the fact **real-world binaries are likely to be vulnerable** to these Wasm-based attacks.

----

![Illustration of potential Wasm attacks (from the article)](./media/wasm-attacks.png)

# PoCs of Wasm new attacks

## Stack-based heap overflow

![Some stack layouts in Wasm (from the Lehmann et al. article)](./media/stacks.png){width=80%}

----

```c
int main() {
    int* heap = malloc(sizeof(int));
    *heap = 0xdeadbeef;
    printf("Value before: 0x%0x\n> ", *heap);
    fflush(stdout);
    char input[20];
    fgets(input, 256, stdin);
    printf("Value after: 0x%0x\n", *heap);
    return 0;
}
```

----

Input of the exploit:

```py
p.sendline(b"A" * 24 + p32(0x00011940) + b"A" * 20 + p32(0x50bada55))
```

Rewriting the heap from the stack is made possible by the absence of unmapped zones, memory permissions, and clear separation between zones.

## Rewriting read-only data

Again, what makes this attack possible is that there are no memory permissions in Wasm. This makes possible to overwrite supposedly constant memory (e.g. variables declared with the `const` keyword in C or other languages) whilst this is not possible in classic binaries.

The final goal of the lab will be to explore these memory mechanisms and create an exploit for rewriting read-only memory!

## Calling function model

### Introduction

Wasm is using a **protected call stack** to store the succession of function calls. This stack is **managed by the runtime** and is **not accessible by the Wasm program**.

. . .

The functions and their parameters must be **declared at loading time** and **cannot be modified**. They are then stored into an index referencing the list of callable functions. The Wasm code is also immutable and not observable at runtime.

. . .

This implicitly **enforces control-flow integrity** (CFI) and **blocks conventional direct code reuse attacks** like return-oriented programming (ROP).

### Direct and indirect calls

When calling a function in Wasm, we don't use an address but **a number referencing the function index** built at loading time. Calls to determined functions are called using a hardcoded index.

. . .

But for the support of C/C++ (or other languages) **function pointers**, indirect calls also exists. They take a **variable index number** to mimic the pointer behavior. The function signature is subject to a check at runtime.

----

```c
void myFunction(int x) {
    printf("Value: %d\n", x);
}

int main() {
    void (*functionPointer)(int);
    functionPointer = &myFunction;
    (*functionPointer)(5);
    return 0;
}
```

### Exploiting indirect calls

The call function index for indirect functions is writable and thus **can be overridden**. This allows for restricted code execution: you can arbitrarily call functions **with the same signature**. You may also partly or totally control the function arguments.

. . .


There is no PoC available for this attack with these slides, but it is clearly possible as shown in this blog post^[https://www.fastly.com/blog/hijacking-control-flow-webassembly].

. . .

To mitigate this, the Wasm target in clang have **support for LLVM CFI** (with the `-fsanitize=cfi` flag), which is way better than the default Wasm CFI (as it is done at compile time).

## The future of Wasm security

### Already improving?

The articles above and others have **raised awareness on the weaknesses of Wasm**. One example of this is the implementation of canaries for Wasm in the Clang / LLVM compilation suite, even when it is still stated on the Wasm website that they are useless.

. . .

Other security mechanisms **may be implemented someday** as stated on the Wasm security page, like memory paging and randomization (similar to ASLR). Sadly, no clear roadmap for these features are available.

### More danger incoming?

Wasm and WASI are **still at their beginnings** and still lack a lot of functionalities, especially within WASI.

. . .

The amount of new functionalities incoming may mean still **new vulnerabilities to come**, either in WASI (more APIs to communicate with, so more possibilities to break out of the sandbox) or inside the Wasm runtime itself (race conditions with multithreading, vulnerabilities with garbage collection...)

### More resources

- An explanation of the indirect_call Wasm mechanism^[https://coinexsmartchain.medium.com/wasm-introduction-part-6-table-indirect-call-65ad0404b003]
- A presentation of Wasm/WASI in the Cloud at the Container Plumbing Days 2023^[https://www.youtube.com/watch?v=SZBgTvjGRXY]
- Containers, Wasm and Confidential Computing^[https://enarx.dev/]
- Using Wasm to simply distribute and modify native executables between platforms^[https://wasmer.io/posts/wasm-as-universal-binary-format-part-1-native-executables]

### The end

Thanks for your attention!

Questions?

### The lab

Now let's explore and practice!

::: nonincremental
- It is advised to work in pairs
- Don't stay stuck, ask for help
- Target for everybody is part 2
:::
